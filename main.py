import json

LOGS = 'proxy_base.json'
REF_URL = "referal.ours.com"
CHECKOUT_URL = "https://shop.com/checkout"

with open(LOGS, 'r') as _file:
    data = json.load(_file)


def find_client():
    possible_clients = {
        e["client_id"]: e["document.referer"]
        for e in data if REF_URL in e['document.referer']
    }

    to_return = {
        "client_id": [],
        "referer": [],
        "ref_code": []
    }
    while data:
        event = data.pop()
        client_id = event["client_id"]
        if client_id in possible_clients and CHECKOUT_URL in event["document.location"]:
            to_return["client_id"].append(client_id)
            to_return["referer"].append(possible_clients[client_id])
            to_return["ref_code"].append(possible_clients[client_id].split('?ref=')[-1])

    return json.dumps(to_return)


print(find_client())
